package com.ruyuan.seckill.domain.dto;


import com.ruyuan.seckill.domain.ShipTemplateChild;
import com.ruyuan.seckill.domain.vo.ShipTemplateChildBuyerVO;

/**
 * 扩展用于与商品相关的属性
 *
 */
public class ShipTemplateChildDTO extends ShipTemplateChild {

    public ShipTemplateChildDTO(ShipTemplateChildBuyerVO shipTemplateChild) {
        this.setFirstPrice(shipTemplateChild.getFirstPrice());
        this.setFirstCompany(shipTemplateChild.getFirstCompany());
        this.setContinuedCompany(shipTemplateChild.getContinuedCompany());
        this.setContinuedPrice(shipTemplateChild.getContinuedPrice());
    }


    /**
     * 用于存放父类的计费方式字段
     */
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }


    @Override
    public String toString() {
        return "ShipTemplateChildDTO{" +
                "type=" + type +
                "} " + super.toString();
    }
}
